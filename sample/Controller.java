package sample;

import com.jfoenix.controls.JFXToggleButton;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import sample.animation.Shake;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Controller {

    @FXML
    private TextField amountBill;

    @FXML
    private ComboBox<?> choiceCurrencyComboBox;

    @FXML
    private JFXToggleButton roundToggle;

    @FXML
    private JFXToggleButton rublesToggle;

    @FXML
    private RadioButton radioButtonFivePercentTip;

    @FXML
    private ToggleGroup tipGroup;

    @FXML
    private RadioButton radioButtonTenPercentTip;

    @FXML
    private TextField paymentAmount;
    @FXML
    private TextField amountRubles;

    @FXML
    private TextField tip;

    @FXML
    private Pane rublesMenu;

    @FXML
    private TextField paymentAmountRub;

    @FXML
    private TextField tipRub;

    @FXML
    private Label errorLabel;

    @FXML
    private Label amountErrorLabel;

    /**
     * Метод, для реализации очищения текстовых полей
     *
     */
    @FXML
    void clear() {
        amountBill.clear();
        tip.clear();
        tipRub.clear();
        paymentAmount.clear();
        paymentAmountRub.clear();
        amountRubles.clear();
        clearLabel(errorLabel);
        clearLabel(amountErrorLabel);
    }

    /**
     * Очищает Label
     */
    private void clearLabel(Label label) {
        if (!label.getText().isEmpty()) label.setText("");
    }

    /**
     * Метод, возвращающий номер выбранной радио кнопки
     *
     * @return радио кнопки
     */

    private int returnChoiceRadioButton() {
        Toggle choice = tipGroup.getSelectedToggle();
        if (choice == radioButtonFivePercentTip) { // 5%
            return 1;
        }
        if (choice == radioButtonTenPercentTip) { // 10%
            return 2;
        }
        return 3; // Без чаевых
    }

    /**
     * Метод, возвращающий номер выбранной валюты
     *
     * @return номер валюты
     */
    @FXML
    private int choiceCurrency() {
        String selectCurrency = (String) choiceCurrencyComboBox.getValue();
        try {
            switch (selectCurrency) {
                case "Рубль ₽":
                    rublesMenu.setVisible(false);
                    rublesToggle.setVisible(false);
                    return 1;
                case "Доллар $":
                    rublesToggle.setVisible(true);
                    return 2;
                case "Евро €":
                    rublesToggle.setVisible(true);
                    return 3;
            }
        } catch (Exception e) {
            errorLabel.setText("Валюта не выбрана!");
            return -1;
        }
        return 0;
    }

    /**
     * Метод форматирует число до двух знаков после запятой
     *
     * @param number число для форматированного вывода
     * @return форматированное до двух знаков число
     */
    private Double formationNumber(Double number) {
        return new BigDecimal(number).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    /**
     * Вызывает метод округления числа до целого, если включен переключатель "Округлить"
     *
     * @param doubleNumbers число, которое необходимо округлить до целого
     * @return округленное до целого число, если включен переключатель или неокругленное число, если выключен переключатель
     */
    private double roundUp(double doubleNumbers) {
        if (checksSelectedSwitch(roundToggle)) {
            if (returnChoiceRadioButton() == 1) {
                if (doubleNumbers % 3 != 0) {
                    doubleNumbers += 0.5;
                }
            }
            return roundOffNumber(doubleNumbers);
        }
        return doubleNumbers;
    }

    /**
     * Округляет числа до целого
     *
     * @param number число, которое необходимо округлить до целого
     * @return округленное до целого число
     */
    private double roundOffNumber(double number) {
        double result;
        result = Math.floor(number);
        return result;
    }

    /**
     * Метод проверяет выбран ли переключатель
     *
     * @param toggleButton переключатель для проверки
     * @return true - выбран, false - не выбран
     */
    @FXML
    private boolean checksSelectedSwitch(JFXToggleButton toggleButton) {
        return toggleButton.isSelected();
    }

    /**
     * Метод, реализующий показ Pane(Скрыта) с выводом в рублях,
     * если включен переключатель "В рубли"
     */
    @FXML
    void visibleRublesMenu() {
        rublesMenu.setVisible(false);
        if (checksSelectedSwitch(rublesToggle)) {
            rublesMenu.setVisible(true);
        }
    }

    /**
     * Метод, производящий подсчет полей: к оплате, чаевые(в зависимости от выбранного процента чаевых).
     *
     * @param amount   сумма чека
     * @param percent  процент чаевых
     * @param currency валюта
     */
    void countTip(double amount, double percent, String currency) {
        DecimalFormat twoCharForm = new DecimalFormat("#0.00");
        double tips = amount * percent;
        amount = tips + amount;
        paymentAmount.setText(twoCharForm.format(formationNumber(roundUp(amount))).replace(",", ".") + currency);
        tip.setText(twoCharForm.format(formationNumber(roundUp(tips))).replace(",", ".") + currency);
    }

    /**
     * Метод, производящий подсчет полей: чаевые в рублях, к оплате в рублях, сумма в рублях.
     *
     * @param amount  сумма чека
     * @param percent процент чаевых
     * @param rate    курс валют
     */
    void countRubles(double amount, double percent, double rate) {
        DecimalFormat twoCharForm = new DecimalFormat("#0.00");
        double tip = amount * percent;
        double toPay = amount + tip;

        paymentAmountRub.setText(twoCharForm.format(formationNumber(roundUp(toPay * rate))).replace(",", ".") + " ₽");
        tipRub.setText(twoCharForm.format(formationNumber(roundUp(tip * rate))).replace(",", ".") + " ₽");
        amountRubles.setText(twoCharForm.format((formationNumber(roundUp(amount * rate)))).replace(",", ".") + " ₽");
    }

    /**
     * Сохраняет введенное значение в текстовое поле "Введите сумма чека"
     *
     * @return введенное число или null, если введены нечисловые данные - ошибка("Введите число")
     */
    private Double savesEnteredValue() {
        try {
            return Double.parseDouble(amountBill.getText().replace(",", "."));
        } catch (Exception e) {
            amountErrorLabel.setText("Введите число!");
        }
        return null;
    }

    /**
     * Проверяет поле ввода суммы чека на наличие введенных данных
     *
     * @return false, если поле пустое и true, если в поле явведены данные
     */
    private boolean checksInputField() {
        if (amountBill.getText().isEmpty()) {
            amountErrorLabel.setText("Не указана сумма чека!");
            return false;
        }
        return true;
    }

    @FXML
    void count() {
        if (amountBill.getText().isEmpty()) {
            Shake valueShake = new Shake(amountBill);
            valueShake.playAnimation();
        }
        if (checksInputField() && savesEnteredValue() != null) {

            double amount = savesEnteredValue();

            if (amount <= 0) {
                amountErrorLabel.setText("Больше нуля!");
            } else {
                // Рубли

                if (choiceCurrency() == 1) {
                    // 5 %
                    if (returnChoiceRadioButton() == 1) {
                        countTip(amount, 0.05, " ₽");
                    }
                    // 10 %
                    if (returnChoiceRadioButton() == 2) {
                        countTip(amount, 0.1, " ₽");
                    }
                    // Без чаевых
                    if (returnChoiceRadioButton() == 3) {
                        countTip(amount, 0, " ₽");
                    }
                }

                // Доллары
                if (choiceCurrency() == 2) {
                    // 5 %
                    if (returnChoiceRadioButton() == 1) {
                        countTip(amount, 0.05, " $");

                        if (checksSelectedSwitch(rublesToggle)) {
                            countRubles(amount, 0.05, 70.76);
                        }
                    }
                    // 10 %
                    if (returnChoiceRadioButton() == 2) {
                        countTip(amount, 0.1, " $");

                        if (checksSelectedSwitch(rublesToggle)) {
                            countRubles(amount, 0.1, 70.76);
                        }
                    }
                    // Без чаевых
                    if (returnChoiceRadioButton() == 3) {
                        countTip(amount, 0, " $");

                        if (checksSelectedSwitch(rublesToggle)) {
                            countRubles(amount, 0, 70.76);
                        }
                    }
                }

                // Евро
                if (choiceCurrency() == 3) {
                    // 5 %
                    if (returnChoiceRadioButton() == 1) {
                        countTip(amount, 0.05, " €");

                        if (checksSelectedSwitch(rublesToggle)) {
                            countRubles(amount, 0.05, 78.65);
                        }
                    }
                    // 10 %
                    if (returnChoiceRadioButton() == 2) {
                        countTip(amount, 0.1, " €");

                        if (checksSelectedSwitch(rublesToggle)) {
                            countRubles(amount, 0.1, 78.65);
                        }
                    }
                    // Без чаевых
                    if (returnChoiceRadioButton() == 3) {
                        countTip(amount, 0, " €");

                        if (checksSelectedSwitch(rublesToggle)) {
                            countRubles(amount, 0, 78.65);
                        }
                    }
                }
            }
        }
    }
}















