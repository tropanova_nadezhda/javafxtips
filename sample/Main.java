package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;


public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("tipCalc.fxml"));
        primaryStage.getIcons().add(new Image("sample\\assets\\tea.jpg"));
        primaryStage.setTitle("На чай");
        primaryStage.setScene(new Scene(root, 450, 600));
        primaryStage.setResizable(false);
        primaryStage.show();
    }
}
